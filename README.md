# devops-netology
# Игнорировать любые каталоги с каталогом .terraform и вложенные файлы в этом (.terraform) каталоге.
**/.terraform/*

# Игнорировать файлы с расширением .tfstate и файлы содержащие такое расширение.  
*.tfstate
*.tfstate.*

# Игнорировать файл crash.log и любое файлы с двойным расширением где первое расширение содержит любые символы.
crash.log
crash.*.log

# Игнорировать любые файлы с расширением .tfvars и .tfvars.json
*.tfvars
*.tfvars.json

# Игнорировать файлы override.tf и override.tf.json а также файлы содержащие в названии _override.tf и _override.tf.json.
override.tf
override.tf.json
*_override.tf
*_override.tf.json


# Игнорировать скрытую директорию .terraformrc и файл terraform.rc
.terraformrc
terraform.rc
